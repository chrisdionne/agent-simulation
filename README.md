# Agent Simulation Game

## Description
In this simulation game, players control a team of agents with various roles and backgrounds. Each agent has a set of attributes and behavioral tendencies that influence their performance in the game. The agents must navigate through an environment filled with challenges and resources, making decisions to survive and thrive.

## Characters
There are four types of agents in the game, each with a unique role and backstory:
- **Alpha**: A human engineer with exceptional problem-solving skills and expertise in machinery. 
- **Beta**: A battle-hardened human soldier trained in advanced warfare and various weapons.
- **Gamma**: An alien pirate mercenary with unconventional skills and a mysterious past.
- **Delta**: An alien bounty hunter known for her unerring precision and relentless pursuit.

## Game Mechanics
Agents need to manage their health, hunger, thirst, and stress levels to survive in the game. They can interact with the environment to find resources and use their unique abilities to overcome challenges. Certain agents have manipulative or greedy traits, adding another layer of complexity to the game.

## How to Run the Game
To run the game, you need to have Python 3.8 or above installed on your machine. Clone the repository, navigate to the project directory, and run the `main.py` script.

```bash
git clone https://gitlab.com/chrisdionne/agentsimulation.git
cd agentsimulation
pip install -r requirements.txt
python3 main.py
```

## License Agreement

### SOFTWARE LICENSE AGREEMENT

1. LICENSE:

This Software License Agreement (the "Agreement") is a legal agreement between You (either an individual or an entity), the end user, and [Your Name or Company Name] (the "Owner"), the author and copyright owner of the enclosed software ("Software"). 

By installing the Software, you agree to be bound by the terms of this Agreement. If you do not agree to the terms of this Agreement, do not install or use the Software.

2. GRANT OF LICENSE:

Subject to the terms below, the Owner hereby grants You a non-exclusive, non-transferable license to install and to use the Software. Under this license, you may: (i) install the Software on as many computers as you wish; (ii) use the Software on one or more computers; (iii) make copies of the Software for backup or archival purposes.

3. COPYRIGHT:

All title, including but not limited to copyrights, in and to the Software and any copies thereof are owned by the Owner or its suppliers. All title and intellectual property rights in and to the content which may be accessed through use of the Software is the property of the respective content owner and may be protected by applicable copyright or other intellectual property laws and treaties. This Agreement grants You no rights to use such content.

4. RESTRICTIONS:

You may not distribute copies of the Software to third parties. You may not reverse engineer, decompile, or disassemble the Software, except and only to the extent that such activity is expressly permitted by applicable law notwithstanding this limitation. 

5. NO WARRANTIES:

The Owner expressly disclaims any warranty for the Software. The Software is provided 'As Is' without any express or implied warranty of any kind, including but not limited to any warranties of merchantability, noninfringement, or fitness of a particular purpose. The Owner does not warrant or assume responsibility for the accuracy or completeness of any information, text, graphics, links or other items contained within the Software. 

6. LIMITATION OF LIABILITY:

In no event shall the Owner be liable for any damages (including, without limitation, lost profits, business interruption, or lost information) rising out of 'Authorized Users' use of or inability to use the Software, even if the Owner has been advised of the possibility of such damages. 

BY INSTALLING THE SOFTWARE, YOU ACKNOWLEDGE THAT YOU HAVE READ AND UNDERSTOOD THIS AGREEMENT AND YOU AGREE TO BE BOUND BY THE AGREEMENT'S TERMS AND CONDITIONS.

## Credits

Samuel .L Jackson (not the actor)
Christopher A. Dionne

