import os
import re
import random
import time
import openai
import pyttsx3
from typing import Dict
from tabulate import tabulate
from dotenv import load_dotenv
from ansi_colors import RESET, CYAN, RED, YELLOW, BLUE, GREEN, WHITE, LIGHT_BLUE, LIGHT_CYAN, LIGHT_GRAY, BLINK, BRIGHT, \
    MAGENTA, LIGHT_GREEN

load_dotenv()

engine = pyttsx3.init()
engine.setProperty('rate', 200)
engine.setProperty('voice', 'english-us')
voices = engine.getProperty('voices')

if len(voices) >= 4:
    alpha_voice = voices[0]
    beta_voice = voices[1]
    gamma_voice = voices[2]
    delta_voice = voices[3]
else:
    print("Less than 4 voices available. Please add more voices or modify the code to use fewer voices.")


class Agent:
    """
    A class representing an agent in the simulation.

    Attributes:
        name (str): The name of the agent.
        team (str): The name of the team that the agent belongs to.
        backstory (str): A brief description of the agent's backstory or role in the simulation.
        max_health (int): The maximum health level that the agent can have.
        max_hunger (int): The maximum hunger level that the agent can have.
        max_thirst (int): The maximum thirst level that the agent can have.
        max_stress (int): The maximum stress level that the agent can have.
        hunger (int): The current hunger level of the agent.
        thirst (int): The current thirst level of the agent.
        stress (int): The current stress level of the agent.
        manipulative (bool): Whether or not the agent is manipulative.
        greedy (bool): Whether or not the agent is greedy.
        alive (bool): Whether or not the agent is alive.
        inventory (dict): A dictionary containing the agent's current inventory of food and water.

    """

    def __init__(self, name, team, characters, max_health, max_hunger, max_thirst, max_stress, manipulative=False,
                 greedy=False, alive=True):
        """
        Initializes a new instance of the Agent class with the given attributes.

        Parameters:
            name (str): The name of the agent.
            team (str): The name of the team that the agent belongs to.
            backstory (str): A brief description of the agent's backstory or role in the simulation.
            max_hunger (int): The maximum hunger level that the agent can have.
            max_thirst (int): The maximum thirst level that the agent can have.
            max_stress (int): The maximum stress level that the agent can have.
            manipulative (bool, optional): Whether or not the agent is manipulative (default False).
            greedy (bool, optional): Whether or not the agent is greedy (default False).
            alive (bool, optional): Whether or not the agent is alive (default True).

        Returns:
            None.

        """
        self.name = name
        self.team = team
        self.role = characters['role']
        self.backstory = characters['backstory']
        self.health = 25
        self.max_health = max_health
        self.max_hunger = max_hunger
        self.max_thirst = max_thirst
        self.max_stress = max_stress
        self.hunger = max_hunger
        self.thirst = max_thirst
        self.stress = max_stress
        self.manipulative = manipulative
        self.greedy = greedy
        self.alive = alive
        self.inventory = {'food': 0, 'water': 0}

    def __str__(self):
        """
        Returns a string representation of the Agent object.

        Returns:
            str: A string representation of the Agent object.

        """
        return f"{self.name} Team: {self.team} Hunger: {self.hunger}, Thirst: {self.thirst}, Stress: {self.stress}, Alive: {self.alive})"

    def eat(self, food):
        """
        Reduces the agent's hunger level by the amount of food eaten.

        Parameters:
            food (int): The amount of food the agent eats.

        Returns:
            None.
        """
        self.hunger = max(0, self.hunger - food)
        self.inventory['food'] -= food

    def drink(self, water):
        """
        Reduces the agent's thirst level by the amount of water drunk.

        Parameters:
            water (int): The amount of water the agent drinks.

        Returns:
            None.
        """
        self.thirst = max(0, self.thirst - water)
        self.inventory['water'] -= water

    def relax(self, relaxation):
        """
        Reduces the agent's stress level by the amount of relaxation.

        Parameters:
            relaxation (int): The amount of relaxation the agent has.

        Returns:
            None.
        """
        self.stress = max(0, self.stress - relaxation)


class Environment:
    """
    A class representing the environment in which agents interact.

    Attributes:
        food (int): The amount of food in the environment.
        water (int): The amount of water in the environment.

    """

    def __init__(self, food, water):
        """
        Initializes a new instance of the Environment class with the given amounts of food and water.

        Parameters:
            food (int): The amount of food in the environment.
            water (int): The amount of water in the environment.

        Returns:
            None.

        """
        self.food = food
        self.water = water

    def simulate_turn(self, agents):
        """
        Simulates a turn in the environment, updating each agent's hunger and thirst levels and
        checking if any agents have died from starvation or dehydration.

        Parameters:
            agents (list): A list of Agent objects representing all agents in the simulation.

        Returns:
            None.
            :param agents:

        """
        for agent in agents:
            if agent.alive:
                food_needed = random.randint(1, 3)
                water_needed = random.randint(1, 3)

                if self.food >= food_needed:
                    agent.hunger -= food_needed
                    self.food -= food_needed
                else:
                    agent.hunger -= self.food
                    self.food = 0

                if self.water >= water_needed:
                    agent.thirst -= water_needed
                    self.water -= water_needed
                else:
                    agent.thirst -= self.water
                    self.water = 0

                if agent.hunger <= 0 or agent.thirst <= 0:
                    agent.alive = False


class TeamClass:
    characters: Dict[str, Dict[str, str]] = {
        "Alpha": {"role": "Human Engineer",
                  "backstory": "Alpha, a Human Engineer, hails from a planet once known as Earth. The last message received from the planet, over a year ago, was a distress signal. The fate of Earth remains uncertain, leaving Alpha with a heavy burden of being possibly the last of his kind. With his exceptional problem-solving skills and expertise in machinery, he is an invaluable asset to the team."},
        "Beta": {"role": "Human Soldier",
                 "backstory": "Beta, a battle-hardened Human Soldier, shares a similar fate with Alpha. The mystery of Earth's silence haunts him. Trained in advanced warfare and a variety of weapons, Beta's combat skills might be the key to the team's survival in this unpredictable environment."},
        "Gamma": {"role": "Alien Pirate Mercenary",
                  "backstory": "Gamma, the Alien Pirate Mercenary, was once a feared figure in the intergalactic criminal underworld. Now, he brings his unconventional skills and cunning to the team. With a past shrouded in secrecy and rumors of treachery and exploits across the galaxy, Gamma's presence adds an edge to the group."},
        "Delta": {"role": "Alien",
                  "backstory": "Delta, an Alien Bounty Hunter, is known for her unerring precision and relentless pursuit. Her past is a saga of countless missions and dangerous encounters. Her tracking skills and survival instincts could be the team's best hope in navigating through unfamiliar territories."}
    }
    teams = ["Team A", "Team B"]

    def __init__(self, name):
        self.name = name
        self.agents = []
        # Here we initialize the characters for this team to an empty dictionary.

    def add_agent(self, agent):
        # When an agent is added, we also add their character info to `team_characters`.
        self.agents.append(agent)
        self.characters[agent.name] = {"role": agent.role, "backstory": agent.backstory}


def create_agents():
    team_a = TeamClass("Team A")
    team_b = TeamClass("Team B")

    agents = []
    for i, (name, character) in enumerate(TeamClass.characters.items()):
        if i < len(TeamClass.characters) // 2:
            team = team_a
        else:
            team = team_b

        manipulative = False
        greedy = False

        if name == "Gamma":  # Manipulative agent
            manipulative = True

        if name == "Delta":  # Greedy agent
            greedy = True

        # The 'character' dictionary now comes from TeamClass.characters
        agent = Agent(name, team, character, 5, 5, 5, manipulative, greedy)
        agents.append(agent)
        team.add_agent(agent)

    return agents, team_a, team_b


class Prompts:
    def __init__(self, agent, environment, other_agents):
        self.prompts = [
            "Limit all dialogue to less than 100 characters long. "
            f"Learn the {agent.backstory} from each {agent.team} use their backstory to influence their dialogue. ",
            "Each agent should contribute to the dialogue, addressing others by name. In separate responses. ",
            f"Agent {agent.name} from {agent.team} is currently in a spaceship. ",
            f"The spaceship's supplies include {environment.food} units of food and {environment.water} units of water. Here's the current state of Agent {agent.name}: {agent}. ",
            f"There are other agents from {agent.team} in the spaceship: {', '.join(other_agents)}. ",
            f"There are no other agents from {agent.team} in the spaceship. ",
            "Considering the following situation, please describe the dialogue and interaction between the characters in the spaceship. Only respond in third person. Your responses influence the dialogue and its detrimental that your very first response to the other agents should not require a response from them. ",
            "The only actions the agents can take are: 'attack', 'share', 'search'. Dont keep asking questions in the dialogue for the actions, just make a choice and include it in the dialogue."
        ]


# Create the agents and teams
agents, team_a, team_b = create_agents()




def generate_action(agent, prompt):
    # Check that the agent has a 'team' attribute
    if not hasattr(agent, 'team'):
        raise AttributeError("'Agent' object has no attribute 'team'")

    openai.api_key = os.getenv("OPENAI_API_KEY")
    # Generate the action
    response = openai.Completion.create(
        engine="text-davinci-003",
        prompt=prompt,
        max_tokens=200,
        temperature=0.3,
        n=1
    )

    action_text = response['choices'][0]['text'].strip()
    pattern = r"(\w+): \"(.*?)\""
    matches = re.findall(pattern, action_text)

    # Initialize an empty dictionary to store the dialogues
    dialogues = []

    # Go through all the matches and store the dialogues
    for agent, dialogue in matches:
        # Add a tuple with the agent and dialogue to the list
        dialogues.append((agent, dialogue))

    # Print the dialogues
    for agent, dialogue in dialogues:
        print(f"{agent:6}: {dialogue}")

    dialogue = dialogues

    return action_text, dialogue


def agent_action(agent, environment, agents):
    """
    Determines and performs an action for a given agent based on the current environment and other agents.

    Parameters:
        agent (Agent): The agent whose action is to be determined.
        environment (Environment): The current state of the environment.
        agents (list): A list of all agents in the simulation.

    Returns:
        action_text, agent_dialogue: The generated action text and dialogue.
    """

    if not agent.alive:
        return None, None

    other_agents = [a.name for a in agents if a.team == agent.team and a.name != agent.name]

    has_interaction = len(other_agents) > 0

    if not has_interaction:
        print(f"{agent.name:15} awaits interaction from {agent.team}.")
        print(f"It's {agent.team}'s turn.")
        return None, None
    else:
        prompts = Prompts(agent, environment, other_agents)

        prompt = ''.join(prompts.prompts[:3] if other_agents else prompts.prompts[:2])
        prompt += ''.join(prompts.prompts[3:])

        action_text, dialogue = generate_action(agent, prompt)

        lower_case_action_text = action_text.lower()

        # engine.say(action_text)
        # engine.runAndWait()

        if "attack" in lower_case_action_text:
            target_agents = [a for a in agents if a.team != agent.team and a.alive]
            if target_agents:
                target_agent = random.choice(target_agents)
                print(f"{CYAN}{'-' * 100}{RESET}")
                print(RED + f"{agent.name} is about to attack {target_agent.name}." + RESET)  # Print in red
                attack(agent, target_agent)

        elif "share" in lower_case_action_text and other_agents:
            # Check if agent has enough resources to share
            if agent.inventory['food'] <= 0 or agent.inventory['water'] <= 0:
                print(f"{agent.name} does not have enough resources to share.")
            else:
                share_resources(agent, other_agents[0], environment.food // 2, environment.water // 2)
                print(GREEN + f"{agent.name} shared {other_agents[0]} with {agent.name}\n" + RESET)  # Print in green
            print(f"{CYAN}{'-' * 100}{RESET}")

        elif "search" in lower_case_action_text:
            search_for_resources(agent, environment.food // 2, environment.water // 2)
            print(f"{CYAN}{'-' * 100}{RESET}")

        return action_text, dialogue


def attack(attacker, target, damage=None):
    """
    Simulates an attack of the attacker agent on the target agent.

    The function reduces the health of the target agent by a random amount and checks if the target agent
    has died as a result of the attack. It also transfers a random amount of food and water from the target to the attacker.

    Parameters:
        attacker (Agent): The agent performing the attack.
        target (Agent): The agent that is being attacked.
        damage (int, optional): The damage dealt by the attacker.

    Returns:
        None.
    """

    # Pad all strings to the same width
    attacker_name = f"{attacker.name}"
    action = "is attacking"
    target_name = f"{target.name}"

    # Concatenate strings and print
    print(RED + ' '.join([attacker_name, action, target_name]) + RESET)

    # Reduce the target's health by a random amount between 1 and 3
    damage = random.randint(1, 3) if damage is None else damage
    target.health -= damage

    # Steal a random amount of food and water from the target
    stolen_food = min(random.randint(1, 3), target.inventory['food'])
    stolen_water = min(random.randint(1, 3), target.inventory['water'])
    target.inventory['food'] -= stolen_food
    target.inventory['water'] -= stolen_water
    attacker.inventory['food'] += stolen_food
    attacker.inventory['water'] += stolen_water

    # Pad all strings to the same width
    target_name = f"{target.name}"
    action = "took"
    damage_str = f"{damage}"
    health_str = f"{target.health}"
    health_action = "health"

    # Concatenate strings and print
    print(RED + ' '.join([target_name, action, damage_str, "damage and now has", health_str, health_action]) + RESET)
    print(f"{attacker.name} stole {stolen_food} food and {stolen_water} water from {target.name}.")

    # Check if the target has died
    if target.health <= 0:
        target.alive = False
        print(RED + f"{target.name} has been killed by {attacker.name}!" + RESET)

    print("{}'s health: {} | {}'s health: {}".format(BLUE + target.name + RESET, GREEN + str(target.health), BLUE + attacker.name + RESET, RED + str(attacker.health) + RESET))

    print(f"{CYAN}{'-' * 100}{RESET}")


def share_resources(sharer, receiver_name, food_share, water_share):
    """
    Shares food and water resources between two agents.

    Parameters:
        sharer (Agent): The agent who is sharing their resources.
        receiver_name (str): The name of the agent receiving the resources.
        food_share (int): The amount of food being shared.
        water_share (int): The amount of water being shared.

    Returns:
        None.
    """
    # Find the agent object that corresponds to receiver_name
    receiver = None
    for agent in agents:
        if agent.name == receiver_name:
            receiver = agent
            break

    if receiver is None:
        print(f"Couldn't find agent with name {receiver_name}!")
        return

    # Check if sharer has enough resources to share
    if sharer.inventory['food'] < food_share or sharer.inventory['water'] < water_share:
        print(f"{sharer.name} does not have enough resources to share.")
        return

    # Share resources
    sharer.inventory['food'] -= food_share
    sharer.inventory['water'] -= water_share

    receiver.inventory['food'] += food_share
    receiver.inventory['water'] += water_share

    print("{} shared {} units of food and {} units of water with {}"
          .format(BLUE + sharer.name + RESET, GREEN + str(sharer.inventory['food']) + RESET,
                  LIGHT_BLUE + str(sharer.inventory['water']) + RESET, BLUE + receiver.name + RESET))

    print("{} has food: {} water: {} and {} has food: {} water: {}"
          .format(BLUE + sharer.name + RESET, GREEN + str(sharer.inventory['food']) + RESET,
                  LIGHT_BLUE + str(sharer.inventory['water']) + RESET,
                  BLUE + receiver.name + RESET, GREEN + str(receiver.inventory['food']) +
                  RESET, LIGHT_BLUE + str(receiver.inventory['water']) + RESET))


def search_for_resources(agent, food, water):
    """
    Simulates an agent searching for resources.

    Parameters:
        agent (Agent): The agent searching for resources.
        food (int): The total amount of food in the environment.
        water (int): The total amount of water in the environment.

    Returns:
        None.
    """
    print(f"{CYAN}{'-' * 100}{RESET}")
    print(f"{BRIGHT}{YELLOW}\n{agent.name} is searching for resources!{RESET}")
    found_food = min(random.randint(1, 3), food)
    found_water = min(random.randint(1, 3), water)
    agent.inventory['food'] += found_food
    agent.inventory['water'] += found_water

    # Collect the results in a list
    results = [[agent.name, found_food, found_water]]

    # Print the results as a table
    print(tabulate(results, headers=['Agent Name', 'Food Found', 'Water Found'], tablefmt='grid'))
    print("{}'s food: {} and water: {}".format(BLUE + agent.name + RESET, GREEN + str(agent.inventory['food']) + RESET, LIGHT_BLUE + str(agent.inventory['water']) + RESET))


def print_initial_state(agents, environment):
    print(f"{LIGHT_BLUE}{'*' * 25} Loading Initial environment resources: {'*' * 53}{RESET}")
    print("Food: {}, Water: {}".format(environment.food, environment.water))
    time.sleep(1)  # Wait for the game to start
    print(f"{LIGHT_CYAN}{'*' * 49} Agent Information {'*' * 50}{RESET}")
    for agent in agents:
        print('Agent', agent.name, agent.alive, agent.inventory['food'])
        time.sleep(1)  # Wait for the game to start


def main():
    print(f"{GREEN}{'*' * 50} Agent Simulation {'*' * 50}{RESET}")
    agents, _, _ = create_agents()  # Capture the agents list
    environment = Environment(food=10, water=10)
    turn_count = 0
    print(f"{BLUE}Created by: Some Random Guys From Reddit{RESET}")
    time.sleep(3)  # Wait for the game to start

    print(f"{WHITE}{'*' * 25} The game has started! The agents are now in the spaceship. {'*' * 33}{RESET}")
    print_initial_state(agents, environment)

    while any(agent.alive for agent in agents):
        print(f"{GREEN}{'-' * 100}{RESET}")
        print(f"{BLUE}Turn {turn_count + 1}{RESET}")
        environment.simulate_turn(agents)  # Simulate resource depletion
        for agent in agents:
            agent_action(agent, environment, agents)

            if agent.hunger <= agent.max_hunger // 4 or agent.thirst <= agent.max_thirst // 4:
                prompt = f"{agent.name} from {agent.team} is low on resources and demands more food and water."
                action_text, _ = generate_action(agent, prompt)
                print(f"{agent.name}: {action_text}\n")

        print(f"Environment resources after this turn: Food: '{environment.food}', Water: '{environment.water}'")
        print("Environment resources after this turn: Food: {} Water: {}".format(environment.food, environment.water))
        turn_count += 1
        time.sleep(1)


if __name__ == "__main__":
    main()
